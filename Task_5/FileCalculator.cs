﻿using Calculat;
namespace task5;

public class FileCalculator
{
    CalculatorLogic calculatorLogic = new CalculatorLogic();
    public void CalculateInFile(string path)
    {
        if (String.IsNullOrEmpty(path))
        {
            throw new ArgumentNullException();
        }
        string[] expressions = File.ReadAllLines(path);
        string newPath = path.Insert(path.Length - 4, ".Answer");

        for (int i = 0; i < expressions.Length; i++)
        {
            if (expressions[i] == string.Empty)
            {
                File.AppendAllText(newPath, $"{i}.=  Empty or error\n");
                continue;
            }

            string answer ="";
            try
            {
                answer = calculatorLogic.Calculate(expressions[i]);
                File.AppendAllText(newPath, $"{i}.=  {answer}\n");
            }
            catch when(String.IsNullOrEmpty(answer))
            {
                File.AppendAllText(newPath, $"{i}.=  Error in expression\n");
                continue;
            }
        }
    }
}
