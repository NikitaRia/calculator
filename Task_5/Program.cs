﻿namespace task5;
class Program
{
    static void Main()
    {
        FileCalculator fileCalculator = new FileCalculator();
        ConsoleCalculator consoleCalculator = new ConsoleCalculator();

        Console.WriteLine("\tuse calculator via console - press 1\n\tuse calculator via file - press 2");

        string chose = Console.ReadLine();

        if (chose == "1")
        {
            Console.WriteLine("Enter expression");
            double result = consoleCalculator.ConsoleCalculation(Console.ReadLine());
            Console.WriteLine(result);
        }
        else if (chose == "2")
        {
            Console.WriteLine("Enter file path");
            fileCalculator.CalculateInFile(Console.ReadLine());
            Console.WriteLine("Done");
        }
        else
        {
            Console.WriteLine("Try once more");
            Console.ReadKey();
            Console.Clear();
            Main();
        }
    }
}