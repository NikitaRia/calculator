﻿using Calculat;
namespace task5;

public class ConsoleCalculator
{
    CalculatorLogic calculatorLogic = new CalculatorLogic();
    public double ConsoleCalculation(string input)
    {
        if (String.IsNullOrEmpty(input))
        {
            return -0;
        }
        try
        {
            input = calculatorLogic.Calculate(input);
            double result = double.Parse(input);
            return result;
        }
        catch when(String.IsNullOrEmpty(input))
        {
            Console.WriteLine("Error input");
            return -0;
        }
    }
}