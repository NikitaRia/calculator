﻿using System;
using System.Collections.Generic;
namespace Calculat
{
    public class CalculatorLogic
    {
        public string Calculate(string input)
        {
            input = Validation(input);
            input = GetPostfixNotation(input);
            input = CalculatePostfixNotation(input);
            return input;
        }
        private string Validation(string input)
        {
            if (String.IsNullOrEmpty(input))
            {
                throw new ArgumentNullException();
            }
            input = RemoveExtraOperators(input);
            int a = 0;
            int b = 0;

            for (int i = 0; i < input.Length; i++)
            {
                if (!char.IsDigit(input[i]) && !IsOperatorOrBracket(input[i]) && input[i] != '.')
                {
                    return String.Empty;
                }
                if (input[i] == '(')
                {
                    a++;
                }
                else if (input[i] == ')')
                {
                    b++;
                }
                if (i == input.Length - 1)
                {
                    if (a != b)
                    {
                        Console.WriteLine("error in writing brackets");
                        return String.Empty;
                    }
                }
                if (i < input.Length - 1 && IsOperator(input[i]))
                {
                    if (IsOperator(input[i + 1]))
                    {
                        Console.WriteLine("error in writing operators");
                        return String.Empty;
                    }
                    if (input[i + 1] == ')')
                    {
                        Console.WriteLine("error in writing operators");
                        return String.Empty;
                    }
                }
                if (i < input.Length - 1 && char.IsDigit(input[i]))
                {
                    if (input[i + 1] == '(')
                    {
                        input = input.Insert(i + 1, "*");
                    }
                }
                if (i < input.Length - 1 && input[i] == ')')
                {
                    if (input[i + 1] == '(')
                    {
                        input = input.Insert(i + 1, "*");
                    }
                }
                if (i < input.Length - 1 && input[i] == ')')
                {
                    if (char.IsDigit(input[i + 1]))
                    {
                        input = input.Insert(i + 1, "*");
                    }
                }
            }
            return input;
        }
        private string GetPostfixNotation(string input)
        {
            if (String.IsNullOrEmpty(input))
            {
                return String.Empty;
            }
            string output = "";
            var operatorStack = new Stack<char>();

            for (int i = 0; i < input.Length; i++)
            {
                if (char.IsDigit(input[i]))
                {
                    bool dot = true;
                    while (!IsOperatorOrBracket(input[i]))
                    {
                        if (char.IsDigit(input[i]) || dot)
                        {
                            output += input[i];
                        }
                        if (input[i] == '.')
                        {
                            dot = false;
                        }

                        i++;

                        if (i == input.Length)
                        {
                            break;
                        }
                    }
                    output += " ";
                    i--;

                }
                if (IsOperatorOrBracket(input[i]))
                {
                    if (operatorStack.Count != 0)
                    {
                        if (input[i] == '(')
                        {
                            operatorStack.Push(input[i]);
                            continue;
                        }
                        if (input[i] == ')')
                        {
                            while (operatorStack.Peek() != '(')
                            {
                                output += operatorStack.Pop() + " ";
                            }
                            operatorStack.Pop();
                            continue;
                        }
                        if (GetPriority(operatorStack.Peek()) < GetPriority(input[i]))
                        {
                            operatorStack.Push(input[i]);
                            continue;
                        }
                        if (GetPriority(operatorStack.Peek()) >= GetPriority(input[i]))
                        {
                            while (operatorStack.Count != 0 && GetPriority(operatorStack.Peek()) >= GetPriority(input[i]))
                            {
                                output += operatorStack.Pop() + " ";
                            }
                            operatorStack.Push(input[i]);
                        }
                    }
                    else
                    {
                        operatorStack.Push(input[i]);
                    }
                }
            }

            while (operatorStack.Count > 0)
            {
                output += operatorStack.Pop() + " ";
            }
            return output;
        }
        private string CalculatePostfixNotation(string input)
        {
            if (String.IsNullOrEmpty(input))
            {
                throw new ArgumentNullException();
            }
            var numbersStack = new Stack<double>();
            double firstValue, secondValue;
            string temp = "";
            char operatr;

            for (int i = 0; i < input.Length; i++)
            {
                if (char.IsDigit(input[i]))
                {
                    while (input[i] != ' ' && !IsOperator(input[i]))
                    {
                        temp += input[i];
                        i++;
                    }
                    numbersStack.Push(double.Parse(temp));
                    temp = "";
                }
                if (IsOperator(input[i]))
                {
                    operatr = input[i];

                    secondValue = numbersStack.Pop();
                    firstValue = numbersStack.Pop();

                    numbersStack.Push(PerformMathOperation(firstValue, secondValue, operatr));
                }
            }
            string result = numbersStack.Pop().ToString();
            return result;
        }
        private bool IsOperator(char chr)
        {
            if ("+-/*^".IndexOf(chr) != -1)
            {
                return true;
            }
            return false;
        }
        private bool IsOperatorOrBracket(char chr)
        {
            if ("+-/*^()".IndexOf(chr) != -1)
            {
                return true;
            }
            return false;
        }
        private byte GetPriority(char chr)
        {
            switch (chr)
            {
                case '(': return 0;
                case '+': return 1;
                case '-': return 1;
                case '*': return 3;
                case '/': return 3;
                case '^': return 4;
                case ')': return 0;
                default: return 6;
            }
        }
        private string RemoveExtraOperators(string input)
        {
            for (int i = 0; i < 10; i++)
            {
                input = input.Replace(',', '.');
                input = input.Replace(" ", "");
                input = input.Replace("++", "+");
                input = input.Replace("--", "-");
                input = input.Replace("//", "/");
                input = input.Replace("**", "*");
                input = input.Replace("^^", "^");
            }
            if (input.Length != 0 && IsOperator(input[0]))
            {
                input = input.Remove(0, 1);
            }
            if (input.Length != 0 && IsOperator(input[input.Length - 1]))
            {
                input = input.Remove(input.Length - 1);
            }
            return input;
        }
        private double PerformMathOperation(double first, double second, char operatOr)
        {
            if(second == 0 && operatOr == '/')
            {
                var exception = new DivideByZeroException();
                Console.WriteLine(exception.Message);
            }
            switch (operatOr)
            {
                case '^': return Math.Pow(first, second);
                case '+': return first + second;
                case '-': return first - second;
                case '*': return first * second;
                case '/': return first / second;

                default: return 0;
            }
        }
        
    }
}
